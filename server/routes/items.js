const express = require("express");
const router = express.Router();
const ItemController = require("../controllers/items");

router.route("/").get(ItemController.get);
router.route("/save").post(ItemController.save);

module.exports = router;
