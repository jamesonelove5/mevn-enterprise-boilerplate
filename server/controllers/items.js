const Item = require("../models/item");

module.exports = {
  async get(req, res) {
    try {
      const items = await Item.find();
      res.send(items);
    } catch (err) {
      res.status(500).send();
    }
  },

  async save(req, res) {
    try {
      let { data } = req.body;
      if (!data || !_.isArray(data) || _.isEmpty(data)) {
        res.status(400).json({ error: "missing Data" });
      }

      const sanitizedData = data.filter(item => {
        if (item.title && item.title.trim() !== "") {
          item.title = item.title.trim();
          return true;
        }
        return false;
      });

      if (_.isEmpty(data)) {
        res.status(400).json({ error: "missing items in Data" });
      }
      const inserted = await Item.insertMany(sanitizedData);
      res.send(inserted);
    } catch (err) {
      res.status(500).send();
    }
  }
};
